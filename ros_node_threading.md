# Node architecture design

## Node principal class
The principal class in the node (the one that implements all the functionality) can be designed in 2 different ways:
- The class inherits from Node
- The class does not inherit from Node, but rather has it as an parameter input in the constructor (this can be useful if the node is composed of several classes like this)

## Building the executable
The executable can be built either by using the "register_component" macro or by using a main.cpp file.
- This way of creating executables is shown in some of the ROS2 tutorials as the new way of working.
- On the other hand, it doesn't provide any insight on how the node is spun (i.e. SingleThread vs. MultiThread executors)

Building the executable using a main.cpp can be as easy as:
```cpp
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    std::shared_ptr<FastenerDetector> node = std::make_shared<FastenerDetector>();

    rclcpp::spin(node);
    rclcpp::shutdown();

    return EXIT_SUCCESS;
}
```

The problem with this design arises if the node constructor is making calls to other ROS services (which generally create callbacks that will be executed only if the node is already spinning, which is not). This causes a **deadlock**

In that case, it is recommended to follow this other architecture, in which the node is passed as a parameter in the class containing the principal functionality (Detector class in this example). To avoid deadlocks the node is spun in a different thread. 


```cpp
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("world_model_tags");

    // Create an executor to spin the node
    std::thread t(
        [node]
        {
            rclcpp::executors::MultiThreadedExecutor executor;
            executor.add_node(node);
            executor.spin();
        });

    world_model_tags::Detector detector(node, dictionary, params);

    // Shutdown
    t.join();
    rclcpp::shutdown();

    return EXIT_SUCCESS;
}

```

Alternatively the node architecture should be design in a fashion that ROS services are never called before the node is spinning (i.e. in the constructor)

## Callback groups
In order to avoid deadlocks it is important to understand how callback groups work. There are 2 types of callback groups:
- **Reentrant**: every callback included in this group is executed in parallel.
- **MutuallyExclusive**: every callback included in this group will be executed sequentially.

Callbacks in different callback group (even if they are MutuallyExclusive) are executed in parallel.

## Waiting for a service to return
Using _spin_until_future_complete_ to wait for a service to finish is not recommended, as it won't work if called inside a callback.

Even if its called inside the main process, this function initialises a new executor, which creates a conflict with the original executor, and no more callbacks will be received.

Instead it is preferred to use **wait_for** (**\__await__()** in python) on the future returned by the async call.


##  References

### Callbacks, deadlocks
- https://docs.ros.org/en/foxy/Concepts/About-Executors.html
- https://docs.ros.org/en/galactic/How-To-Guides/Using-callback-groups.html
- https://karelics.fi/deadlocks-in-rclpy/
- https://discourse.ros.org/t/my-ros2-minimal-examples-difficulties-coming-from-ros1/18705/13


### General ROS2 design guidelines
- https://discourse.ros.org/t/code-smells-in-ros-code/19905/2
- https://discourse.ros.org/t/guidelines-on-how-to-architect-ros-based-systems/12641/10
- https://github.com/S2-group/icse-seip-2020-replication-package/blob/master/ICSE_SEIP_2020.pdf
