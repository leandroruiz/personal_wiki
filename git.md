# Sync local folder with new repo (cheatsheet)
The situation is that we have a local folder with a new project and we want to uploaded to a newly created repo. To do that, go to the local folder and run the following code:

```bash
git init
git checkout -b <branch_name> # usually gitlab creates a 'main' branch by default
git add .
git commit -m <message>
git remote add origin <url>
git remote -v
git push origin <branch_name>
```