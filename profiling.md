# How to profile an executable

A simple way of profiling a C++application is using __perf__ and __hotspot__

To install both tools, run these commands:

```bash
sudo apt install linux-tools-common linux-tools-generic linux-tools-`uname -r`
sudo apt install hotspot
```

Also, it is interesting to add this option in CMakeLists.txt file, to have access to backtraces:

```cmake
add_compile_options(-g)
```

Then, run the test or launcher or whatever, and in a second terminal run (replacing __dynamic_task_planner__ by the name of the process to be examined):
```bash
pidof dynamic_task_planner | xargs -I{} perf record --call-graph dwarf --pid {}
```

After the test is run, just launch __hotspot__
