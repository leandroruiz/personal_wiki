# Tricks to create a nice CMakeLists.txt

- The interfaces item must be named after ${PROJECT_NAME}, i.e the interfaces must have the same name as the package. Otherwise there will be some issues with the libraries and nodes using thos interfaces
    - https://github.com/ros2/rosidl/issues/441#issuecomment-591025515