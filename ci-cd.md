# Help to pass CI/CD tasks
This guide will help verify file format before executing CI/CD tasks.

## File Format

The idea is to go the workspace source folder (~/workspace/src), and add there 2 files:
- [.clang-format](https://gitlab.com/mtdi/pi9419/continuous-integration/-/blob/master/image-builder/docker/clang-format/.clang-format)
- [.cmake-format.json](https://gitlab.com/mtdi/pi9419/continuous-integration/-/blob/master/image-builder/docker/cmake-format/.cmake-format.json)

Those files will be used by the tools to check the source code, and have been extracted from CI/CD repo

Then, it is useful to run the following commands from the terminal inside VSCode (using the ROS extension), as it will allow you to open the files directly on the error line just bi Ctrl+Clicking on the console error output.

### Clang-format
```bash
find -iname *.h -o -iname *.cpp | xargs clang-format -i --verbose -style=file
```

### CMake-format
```bash
find . -iname CMakeLists.txt | xargs cmake-format -i -c .cmake-format.json -l debug
```

### Python
```bash
autopep8 -i -r -v --max-line-length 120 .

# autoflake8 -i -r -v --max-line-length 120 .
flake8 --max-line-length 120 --statistics --show-source .
```

### Markdown
```bash
find . -type f \( -iname \*.md \) -not \( -path "/ac/.ci_config*" \)  -print0 | xargs -0 -P 16 -n1  sh -c 'if [ -f "${1}" ]; then mdl -r ~MD013,~MD014 "$1" 2>&1; fi' --
```

### Generic File format
```bash
find . -type f \( -iname \*.py -o -iname \*.txt -o -iname \*.cpp -o -iname \*.h -o -iname \*.md -o -iname \*.sh -o -iname \*.bash -o -iname \*.xml -o -iname \*.launch \) -not \( -path "/ac/.ci_config*" \)  -print0 | xargs -0 -P 16 -n1  sh -c 'if [ -f "${1}" ]; then LC_ALL=C grep --color=always -inHE "^.*[[:blank:]]+$" "$1" || true; fi' --
```

### Bash file format
```
find . -type f -name '*.bash' -exec shellcheck -s bash {} + || RET=1
```

## How to reference dependencies correctly

### package.xml
Search the correct name for the package in this list:

[rosdep/base.yaml](https://github.com/ros/rosdistro/blob/1a9fda299e71f1b0dbe670516420fb05986b7214/rosdep/base.yaml)

### CMakeLists.txt
Once we know the name of the package (both for package.xml and for our OS package manager), we can run:

```bash
dpkg -L <package>
```
It will return all the files and folders belonging to that package. Just search the one that has the form _\<name\>Config.cmake_ and that will be the name of that we have to use in CMakeLists.txt

```cmake
find_package(name REQUIRED)
```

